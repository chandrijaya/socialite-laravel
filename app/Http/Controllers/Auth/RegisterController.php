<?php

namespace App\Http\Controllers\Auth;

Use App\Models\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Validator;

class RegisterController extends Controller
{
    use RegistersUsers;

    public function index()
    {
        return view('auth.register');
    }

    /**
     * @SWG\Post(
     *   path="/api/post-register",
     *   summary="Post Register",
     *   tags={"auth"},
     *   operationId="register",
     *   @SWG\Response(response=200, description="successful operation"),
     *   @SWG\Response(response=406, description="not acceptable"),
     *   @SWG\Response(response=500, description="internal server error"),
	 *	 @SWG\Parameter(
     *      name="body",
     *      in="body",
     *      description="User email used to create account.",
     *      required=true,
     *      @SWG\Schema(
     *          @SWG\Property(property="email", type="string", example="user123@example.com"),
     *          @SWG\Property(property="password", type="string", example="admin123"),
     *          @SWG\Property(property="name", type="string", example="user123"),
     *          @SWG\Property(property="no_mobile", type="string", example="01234567891")
     *      ),
     *   )
     * )
     */
    public function Register(Request $request)
    {
        $request->validate([
            'name' => ['required'],
            'email' => ['required', 'email', 'unique:users,email'],
            'no_mobile' => ['required','regex:/^([0-9\s\-\+\(\)]*)$/','unique:users,no_mobile','min:10'],
            'password' => ['required', 'min:6']
        ]);

        $daftar = new User();
        $daftar->name = $request->name;
        $daftar->email = $request->email;
        $daftar->no_mobile = $request->no_mobile;
        $daftar->password = bcrypt($request->password);

        $daftar->save();

        if ($daftar) {
            if ($request->wantsJson()) {
                return response()->json([
                    'success' => true,
                    'data' => $daftar
                ], 200);
            } else {
                return redirect()->intended('home')->withSuccess('Great! You have Successfully Logged In'); // Return untuk web
            }
        }
    }
}

