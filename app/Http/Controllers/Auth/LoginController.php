<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use JWTAuth;

class LoginController extends Controller
{
    use AuthenticatesUsers;

    public function index()
    {
        return view('auth.login');
    }

    /**
     * @SWG\Post(
     *   path="/api/post-login",
     *   summary="Post Login",
     *   operationId="login",
     *   tags={"auth"},
     *   @SWG\Response(response=200, description="successful operation"),
     *   @SWG\Response(response=406, description="not acceptable"),
     *   @SWG\Response(response=500, description="internal server error"),
	 *	 @SWG\Parameter(
     *      name="body",
     *      in="body",
     *      description="User credential used to login.",
     *      required=true,
     *      @SWG\Schema(
     *          @SWG\Property(property="no_mobile", type="string", example="01234567891"),
     *          @SWG\Property(property="password", type="string", example="admin123")
     *      ),
     *   )
     * )
     */
    public function Login(Request $request)
    {
        $input = $request->only('no_mobile', 'password');
        $token = null;

        if (!$token = JWTAuth::attempt($input)) {
            if ($request->wantsJson()) {
                return response()->json([
                    'success' => false,
                    'message' => 'Invalid Email or Password',
                ], 401);
            } else {
                return response(null, 401); // Return untuk web
            }
        }

        if ($request->wantsJson()) {
            return response()->json([
                'success' => true,
                'token' => $token,
            ]);
        } else {
            return view('layouts.index');
        }
        // return redirect()->intended('home'); // Return untuk web
    }
}
