<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use JWTAuth;

class LogoutController extends Controller
{
    /**
     * @SWG\Post(
     *   path="/api/logout",
     *   summary="Post Logout",
     *   tags={"auth"},
     *   operationId="logout",
     *   @SWG\Response(response=200, description="successful operation"),
     *   @SWG\Response(response=406, description="not acceptable"),
     *   @SWG\Response(response=500, description="internal server error"),
     *   security={{"Bearer":{}}}
     * )
     */
    public function __invoke(Request $request)
    {
        if(JWTAuth::getToken())
        {
            JWTAuth::invalidate(JWTAuth::getToken());
            if ($request->wantsJson()) {
                return response()->json([
                    'status' => true,
                    'message' => 'log out successfully'
                ], 200);
            } else {
                return Redirect('login'); // Return untuk web
            }
        }

        return response()->json([
            'status' => false,
            'message' => 'you need to login first'
        ], 200);

    }
}
