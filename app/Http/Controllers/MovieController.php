<?php

namespace App\Http\Controllers;

use App\Models\Movie;
use Illuminate\Http\Request;

class MovieController extends Controller
{
    /**
     * @SWG\Get(
     *   path="/api/movie",
     *   summary="Get Movies",
     *   operationId="Movie",
     *   tags={"movie"},
     *   security={{"Bearer":{}}},
     *   @SWG\Response(response=200, description="successful operation"),
     *   @SWG\Response(response=406, description="not acceptable"),
     *   @SWG\Response(response=500, description="internal server error"),
     * )
     */
    public function index()
    {
        $data = Movie::all();

        if(empty($data))
        {
            return response()->json([
                'status' => false,
                'message' => 'movies not found'
            ], 200);

        }

        return response()->json([
            'status' => true,
            'message' => 'get movies',
            'data' => $data
        ], 200);
    }

    /**
     * @SWG\Post(
     *   path="/api/post-movie",
     *   summary="Post Movie Information",
     *   operationId="movie-store",
     *   tags={"movie"},
     *   security={{"Bearer":{}}},
     *   @SWG\Response(response=200, description="successful operation"),
     *   @SWG\Response(response=406, description="not acceptable"),
     *   @SWG\Response(response=500, description="internal server error"),
	 *	 @SWG\Parameter(
     *      name="body",
     *      in="body",
     *      description="User store movie information.",
     *      required=true,
     *      @SWG\Schema(
     *          @SWG\Property(property="movie_title", type="string", example="Planet of Apes"),
     *          @SWG\Property(property="year", type="string", example="1999")
     *      ),
     *   )
     * )
     */
    public function Store(Request $request)
    {
        $request->validate([
            'movie_title' => ['required', 'unique:movies,movie_title'],
            'year' => ['required','regex:/^([0-9\s\-\+\(\)]*)$/','min:4']
        ]);

        $daftar = new Movie();
        $daftar->movie_title = $request->movie_title;
        $daftar->year = $request->year;

        $daftar->save();

        if ($daftar) {
            if ($request->wantsJson()) {
                return response()->json([
                    'success' => true,
                    'data' => $daftar
                ], 200);
            } //else {
                // Return untuk web
            // }
        }
    }
}
