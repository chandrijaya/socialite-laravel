<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddSeatsToTheatresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('theatres', function (Blueprint $table) {
            $table->bigInteger('seat_id')->unsigned()->nullable();
            $table->foreign('seat_id')->references('id')->on('theatres')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('theatres', function (Blueprint $table) {
            //
        });
    }
}
