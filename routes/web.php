<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::namespace('Auth')->group(function() {
    Route::get('register', 'RegisterController@index');
    Route::post('post-register', 'RegisterController@Register');
    Route::get('login', 'LoginController@index');
    Route::post('post-login', 'LoginController@Login'); 
    Route::post('logout', 'LogoutController');

    //Untuk auth menggunakan socialite
    Route::get('auth/{provider}', 'SocialiteController@redirectToProvider');
    Route::get('auth/{provider}/callback', 'SocialiteController@handleProviderCallback');
});

Route::get('home', 'HomeController@index'); 

Auth::routes();

//template buat route yg dilindungi middleware (sementara)
Route::middleware('auth')->group(function() { 
    Route::get('/route-1', 'RoleController@show1')->middleware('roles:0');
    Route::get('/route-2', 'RoleController@show2')->middleware('roles:1');
    Route::get('/route-3', 'RoleController@show3')->middleware('roles:0,1');
});

Route::get('/home', 'HomeController@index')->name('home');
