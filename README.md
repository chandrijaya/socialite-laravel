<h1>Mini Project</h1>

<p>Pada mini project kali ini, teman teman akan dibagi ke dalam beberapa kelompok untuk membuat web API sederhana.
Setiap kelompok dibebaskan untuk merancang ide mengenai kegunaan dan fitur apa saja yang ada di dalam web API sederhana ini. Intinya terserah mau buat apa di project ini.</p>

Namun web tersebut harus memiliki fitur fitur sebagai berikut :

1. Fitur register -> Done!
2. Fitur login menggunakan No HP dan password, dan gunakan konsep JWT -> Done!
3. Fitur login dengan social media ( facebook dan twitter) -> Facebook done, but Twitter.
4. Mohon perhatikan juga hak akses ke setiap route dengan menjaganya menggunakan middleware. -> Initial config . . .

<strong>Catatan: Mungkin buat diagram ERD nya terlebih dahulu untuk kerangka crud-nya. Agar lebih jelas tabel-tabel yang akan dibutuhkan.</strong>

Cara pengumpulan tugas :

1. Link gitlab yang dikumpulkan hanya 1 link 1 kelompok
2. Buat juga 2 video : video demo aplikasi dan video pembahasan mengenai kode yang dibuat (singkat saja). Dan video tersebut simpan juga di dalam repo tersebut
3. Set juga mubarok.iqbal sebagai maintener di repo tersebut

<p><strong>Tujuan diadakan mini project adalah peserta terbiasa bekerja secara tim, sehingga tidak boleh ada peserta yang tidak berkontribusi. Penilaian kelompok akan dinilai berdasarkan ide dan kelengkapan tugas. Penilain individu akan dinilai berdasarkan jumlah line kode dan total commit yang dibuat.</strong></p>